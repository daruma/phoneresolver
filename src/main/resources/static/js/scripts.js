const API_URL = '/phone-resolver/api/v1/phone/';
const PHONE_CODE_INPUT = document.getElementById("phone_code");
const INFO_PANEL = document.getElementById('info_panel');

function fetchCountry() {
    if (!PHONE_CODE_INPUT.value) {
        showErrorPanel('Input field can\'t be empty!');
        return;
    }
    const param = PHONE_CODE_INPUT.value.replace(/\b00/gi, "+");
    return fetch(API_URL + param, {credentials: 'same-origin', mode: 'same-origin', method: "get"})
        .then(resp => {
            if (resp.status === 200) {
                return resp.json();
            } else {
                console.log("Status: " + resp.status);
                return Promise.reject(resp.json().then(data => data['message']));
            }
        })
        .then(result => {
            if (result.length === 0) {
                showErrorPanel('Country not found. Check the phone code entered.');
                return;
            }
            showSuccessPanel(result);
        })
        .catch(err => {
            err.then(message => message ? showErrorPanel(message) : showErrorPanel('Unexpected error!'));
        });
}

document.getElementById('submit_phone_code').addEventListener("click", fetchCountry);
INFO_PANEL.addEventListener("click", hidePanel);

function showErrorPanel(errorMessage) {
    showPanel(errorMessage, 'error');
}

function showSuccessPanel(result) {
    showPanel(result, 'success');
}

function showPanel(message, state) {
    INFO_PANEL.innerText = message;
    INFO_PANEL.setAttribute('class', state);
}

function hidePanel() {
    INFO_PANEL.removeAttribute('class')
}
