package com.neotech.PhoneResolver.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Getter
public final class PhoneCodesMapProvider {
    private final Map<String, List<String>> countryMap;
}
