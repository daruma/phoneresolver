package com.neotech.PhoneResolver.service;

import com.neotech.PhoneResolver.models.PhoneCodesMapProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Service
public class PhoneCodeResolutionService {
    private final PhoneCodesMapProvider provider;

    @Autowired
    public PhoneCodeResolutionService(PhoneCodesMapProvider provider) {
        this.provider = provider;
    }

    @Cacheable(value = "countries")
    public List<String> getCountry(String phoneCode) {
        String spaceReplacedCode = phoneCode.replaceAll(" ", "");
        return provider.getCountryMap().entrySet().stream()
                .filter(entry -> entry.getValue().contains(spaceReplacedCode))
                .map(Map.Entry::getKey)
                .collect(toList());
    }
}
