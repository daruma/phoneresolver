package com.neotech.PhoneResolver.provider;

import com.neotech.PhoneResolver.models.PhoneCodesMapProvider;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toMap;

@Slf4j
public class CountrySourceReader {
    private static final int PHONE_CODE_INDEX = 1;
    private static final int COUNTRY_TITLE_INDEX = 0;

    public PhoneCodesMapProvider readSourcePage(String url, String phoneCodePattern) {
        log.info("Loading phone codes data from " + url);
        try {
            Document document = Jsoup.connect(url).get();
            Map<String, List<String>> countryMap = Parser.parser(Pattern.compile(phoneCodePattern))
                    .table(document).tableRows().countryMap();

            log.info("Data read successfully! Collection size " + countryMap.size());

            return new PhoneCodesMapProvider(countryMap);
        } catch (IOException e) {
            throw new DocumentParseException("Failed to get country code source page", e);
        }
    }

    static class Parser {
        private Element table;
        private Elements tableRows;
        private final Pattern pattern;

        private Parser(Pattern pattern) {
            this.pattern = pattern;
        }

        static Parser parser(Pattern pattern) {
            return new Parser(pattern);
        }

        Parser table(Document document) {
            table = document.select("table").get(1);
            return this;
        }

        Parser tableRows() {
            if (table == null) {
                throw new DocumentParseException("Failed to parse country code table");
            }
            tableRows = table.select("tbody").first().select("tr");
            return this;
        }

        Map<String, List<String>> countryMap() {
            if (tableRows == null) {
                throw new DocumentParseException("Failed to parse country code table rows");
            }
            return IntStream.range(1, tableRows.size())
                    .mapToObj(tableRows::get)
                    .map(tableRow -> tableRow.select("td"))
                    .map(createEntry())
                    .filter(entry -> Strings.isNotEmpty(entry.getKey()))
                    .collect(toMap(AbstractMap.SimpleEntry::getKey, entry -> processPhoneCodes(entry.getValue())));
        }

        private Function<Elements, AbstractMap.SimpleEntry<String, String>> createEntry() {
            return tdata -> new AbstractMap.SimpleEntry<>(getCountryTitle(tdata), getPhoneCode(tdata));
        }

        private String getPhoneCode(Elements tableData) {
            return tableData.get(PHONE_CODE_INDEX).text();
        }

        private String getCountryTitle(Elements tableData) {
            return tableData.get(COUNTRY_TITLE_INDEX).text();
        }

        List<String> processPhoneCodes(String phoneCodes) {
            String suppressed = phoneCodes.replaceAll(" ", "");
            Matcher matcher = pattern.matcher(suppressed);
            List<String> phoneCodesList = new ArrayList<>();
            while (matcher.find()) {
                phoneCodesList.add(matcher.group());
            }
            return phoneCodesList;
        }
    }
}
