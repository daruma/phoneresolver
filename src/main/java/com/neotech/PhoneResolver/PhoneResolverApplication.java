package com.neotech.PhoneResolver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhoneResolverApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhoneResolverApplication.class, args);
	}

}
