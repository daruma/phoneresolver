package com.neotech.PhoneResolver.validation;

import org.springframework.beans.factory.annotation.Value;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class PhoneCodeValidator implements ConstraintValidator<PhoneCodeConstraint, String> {
    private Pattern pattern;
    @Value("${com.neotech.phone.code.validation.pattern}") private String phoneCodePattern;

    @Override
    public void initialize(PhoneCodeConstraint constraint) {
        if(phoneCodePattern == null){
            throw new IllegalArgumentException("Missing pattern for phone codes validation. Check your app config.");
        }
        pattern = Pattern.compile(phoneCodePattern);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return pattern.matcher(value.replaceAll(" ","")).matches();
    }
}
