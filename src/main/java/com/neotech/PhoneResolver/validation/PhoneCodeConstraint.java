package com.neotech.PhoneResolver.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({PARAMETER, FIELD, METHOD})
@Retention(RUNTIME)
@Constraint(validatedBy = PhoneCodeValidator.class)
@Documented
public @interface PhoneCodeConstraint {
    String message() default "Phone code format is not valid. Check examples below.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
