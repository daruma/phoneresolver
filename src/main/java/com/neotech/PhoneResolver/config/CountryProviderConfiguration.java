package com.neotech.PhoneResolver.config;

import com.neotech.PhoneResolver.models.PhoneCodesMapProvider;
import com.neotech.PhoneResolver.provider.CountrySourceReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class CountryProviderConfiguration {

    @Bean
    public PhoneCodesMapProvider getPhoneCodesMap(@Value("${com.neotech.country.source.url}") String url,
                                                  @Value("${com.neotech.phone.code.processing.pattern}") String phoneCodePattern){
        return new CountrySourceReader().readSourcePage(url, phoneCodePattern);
    }
}
