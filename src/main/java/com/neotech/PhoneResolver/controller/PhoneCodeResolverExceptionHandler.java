package com.neotech.PhoneResolver.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;

@ControllerAdvice
public class PhoneCodeResolverExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<ErrorResponse> methodArgumentNotValid(ConstraintViolationException e) {
        String message = new ArrayList<>(e.getConstraintViolations()).get(0).getMessageTemplate();
        return ResponseEntity.badRequest().body(new ErrorResponse(message));
    }

    @AllArgsConstructor
    @Getter
    private static class ErrorResponse {
        private String message;

    }
}
