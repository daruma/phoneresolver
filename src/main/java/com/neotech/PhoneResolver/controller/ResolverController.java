package com.neotech.PhoneResolver.controller;

import com.neotech.PhoneResolver.service.PhoneCodeResolutionService;
import com.neotech.PhoneResolver.validation.PhoneCodeConstraint;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path="/api/v1")
@Validated
@Slf4j
public class ResolverController {
    private final PhoneCodeResolutionService resolutionService;

    @Autowired
    public ResolverController(PhoneCodeResolutionService resolutionService) {
        this.resolutionService = resolutionService;
    }

    @GetMapping(path = "/phone/{phoneCode}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> defineCountry(@PathVariable("phoneCode") @PhoneCodeConstraint String phoneCode){
        log.info("Phone code queried: " + phoneCode);
        List<String> countries = resolutionService.getCountry(phoneCode);
        return ResponseEntity.ok(countries);
    }
}
