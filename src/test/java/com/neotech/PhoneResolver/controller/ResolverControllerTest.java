package com.neotech.PhoneResolver.controller;

import com.neotech.PhoneResolver.config.CountryProviderConfiguration;
import com.neotech.PhoneResolver.service.PhoneCodeResolutionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest({ResolverController.class, PhoneCodeResolutionService.class, CountryProviderConfiguration.class})
public class ResolverControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void defineCountry() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/v1/phone/+3906698").contentType(MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();

        assertThat(result.getResponse().getContentAsString(), is("[\"Vatican City State (Holy See)\"]"));
    }

    @Test
    public void defineCountryConstraintViolation() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/v1/phone/+390-6698").contentType(MediaType.TEXT_PLAIN_VALUE))
                .andExpect(status().is(400))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();

        assertThat(result.getResponse().getContentAsString(), is("{\"message\":\"Phone code format is not valid. Check examples below.\"}"));
    }
}