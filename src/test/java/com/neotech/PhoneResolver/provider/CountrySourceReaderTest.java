package com.neotech.PhoneResolver.provider;

import com.neotech.PhoneResolver.models.PhoneCodesMapProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {CountrySourceReader.class})
@TestPropertySource("classpath:test.properties")
public class CountrySourceReaderTest {

    @Autowired
    private CountrySourceReader reader;
    @Value("${com.neotech.phone.code.processing.pattern}") private String phoneCodePattern;
    @Value("${com.neotech.country.source.url}") private String sourcePageUri;

    @Before
    public void setUp() {
    }

    @Test
    public void processPhoneCodes(){
        CountrySourceReader.Parser parser = CountrySourceReader.Parser.parser(Pattern.compile(phoneCodePattern));

        assertThat(parser.processPhoneCodes("+1 789, chara +456790"), contains(is("+1789"), is("+456790")));
    }

    @Test
    public void readSourceStream(){
        PhoneCodesMapProvider provider = reader.readSourcePage(sourcePageUri, phoneCodePattern);

        assertThat(provider.getCountryMap().size(), is(280));
        assertThat(provider.getCountryMap().get("Ascension"), contains(is("+247")));
    }
}