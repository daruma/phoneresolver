package com.neotech.PhoneResolver.service;

import com.neotech.PhoneResolver.config.CachManagerConfiguration;
import com.neotech.PhoneResolver.config.CountryProviderConfiguration;
import com.neotech.PhoneResolver.models.PhoneCodesMapProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsEmptyCollection.empty;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {PhoneCodeResolutionService.class, CountryProviderConfiguration.class, CachManagerConfiguration.class})
@TestPropertySource("classpath:test.properties")
public class PhoneCodeResolutionServiceTest {

    @Autowired
    private PhoneCodeResolutionService service;
    @Autowired
    private PhoneCodesMapProvider provider;


    @Test
    public void getCountry_Cached() {
        List<String> rusBefore1991 = service.getCountry("+7");
        List<String> rusAfter1991 = service.getCountry("+7");
        assertThat(rusBefore1991 == rusAfter1991, is(true));
    }

    @Test
    public void getCountry_ParamWithSpaces() {
        assertThat(service.getCountry("+390 6698"), contains(is("Vatican City State (Holy See)")));
    }

    @Test
    public void getCountry_CountryGotten() {
        assertThat(service.getCountry("+7"), contains(is("Russia")));
        assertThat(service.getCountry("+390 6698"), contains(is("Vatican City State (Holy See)")));
        assertThat(service.getCountry("+1"), containsInAnyOrder(is("United States"), is("Canada")));
        assertThat(service.getCountry("+5 4545"), empty());
    }
}