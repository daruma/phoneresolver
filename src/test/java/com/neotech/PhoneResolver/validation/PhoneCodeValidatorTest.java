package com.neotech.PhoneResolver.validation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.reflect.Whitebox;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {PhoneCodeValidator.class})
@TestPropertySource("classpath:test.properties")
public class PhoneCodeValidatorTest {

    private PhoneCodeValidator validator;
    @Value("${com.neotech.phone.code.validation.pattern}") private String phoneCodePattern;

    @Before
    public void setUp() {
        validator = new PhoneCodeValidator();
        Whitebox.setInternalState(validator, "phoneCodePattern", phoneCodePattern);
        validator.initialize(mock(PhoneCodeConstraint.class));
    }

    @Test
    public void testIsValidTrue() {
        assertThat(validator.isValid("+8990909", null), is(true));
        assertThat(validator.isValid("+896", null), is(true));
        assertThat(validator.isValid("+894", null), is(true));
        assertThat(validator.isValid("+1894", null), is(true));
        assertThat(validator.isValid("001894", null), is(true));
    }

    @Test
    public void testIsValidFalse() {
        assertThat(validator.isValid("+18791879", null), is(false));
        assertThat(validator.isValid("+01879", null), is(false));
        assertThat(validator.isValid("1879", null), is(false));
        assertThat(validator.isValid("+0001879", null), is(false));
        assertThat(validator.isValid("+00018791218", null), is(false));
        assertThat(validator.isValid("+00o1879", null), is(false));
        assertThat(validator.isValid("+00 1879-76", null), is(false));
    }

    @Test(expected = IllegalArgumentException.class)
    public void initialize(){
        new PhoneCodeValidator().initialize(null);
    }
}